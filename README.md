# Interekt (INTERactive Exploration of Kinematics and Tropisms)

![logo](https://forgemia.inra.fr/felix.hartmann/interekt/-/raw/master/img/logo_Interekt.svg "Interekt logo")


The software Interekt analyses sequences of images taken during plant growth experiments. It can semi-automatically extract the skeleton of one-dimensional organs like shoots, roots, or hypocotyls. Then, the whole motion can be described quantitatively.

A few models of plant tropism (e.g. [AC](https://doi.org/10.1073/pnas.1214301109) and [ACĖ](https://doi.org/10.3389/fpls.2014.00136)) are implemented in Interekt. They can be used to estimate the graviceptive and proprioceptive sensitivities of plants from the motion tracking.

The following book chapter describes in detail how to quantify shoot gravitropism and postiure control using Interekt:

* Hartmann F.P., Chauvet-Thiry H. *et al.* (2022) Methods for a Quantitative Comparison of Gravitropism and Posture Control Over a Wide Range of Herbaceous and Woody Species. In *Plant Gravitropism*, Humana, New York. https://hal.inrae.fr/hal-03400916

![Screenshot](https://forgemia.inra.fr/felix.hartmann/interekt/-/raw/master/img/Screenshot1.png "screenshot of Interekt user interface")

# Install

Interekt is written in the Python language, with a graphical user interface in Tkinter.

For Mac and Windows users, the simplest way to get Python and all the required libraries installed is to download the Anaconda distribution of Python:
https://www.anaconda.com/products/individual

The Anaconda solution is also convenient for Linux users.

## Python libraries required

* Numpy
* Scipy
* Pandas
* Matplotlib
* Scikit-image
* h5py
* OpenCV (optional, but increases processing speed)

# Run

## Windows

Double click on the `Windows_Interekt.bat` file.

## On Linux or Mac

Open a terminal, go to the Interet directory and run `python ./interekt.py`.

# Acknowledgments

The Interekt logo has been artfully designed by [Kam](http://kamsblog.org/) and Marianne Lang.	